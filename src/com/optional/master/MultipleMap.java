package com.optional.master;

import java.util.Optional;

public class MultipleMap {

    public static void main(String[] args) {
        //Possibles inputs
        Integer input = 123456;
        //Integer input = 123456789;
        //Integer input = 1234567891;
        //Integer input = null;

        System.out.println("Before java8 : " + beforeJava8(input));
        System.out.println("After java8 : " + afterJava8(input));
    }
    
    // add trailing zeros to make 10 digits
    public static Integer beforeJava8(Integer input) {
        Integer result;
        if (input != null) {
            int noOfZeros = 10 - (String.valueOf(input).length());//find no of digit missing
            Double multiplier = Math.pow(10, noOfZeros);// convert multiples of 10
            result = input * Math.max(multiplier.intValue(), 1);//multiply
        } else {
            result = null;
        }
        return result;
    }

    // re-written in Java8
    public static Integer afterJava8(Integer input) {
       return Optional.ofNullable(input) // map() returns 'stream object' so cascade any no of times
        .map(e -> Integer.valueOf(10 - (String.valueOf(e).length()))) // this output will be passed to next function()
        .map(noOfZeros -> Math.pow(10, noOfZeros)) // this output will be passed to next function()
        .map(multiplier -> input * Math.max(multiplier.intValue(), 1)) // since last function(), result will be returned
                                                                       // you can use outside variable within in stream
        .orElse(null); // terminating stream
    }
}
// Expected output
//Before java8 : 1234560000
//After java8 : 1234560000

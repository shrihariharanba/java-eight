package com.optional.master;

import java.util.Optional;

public class IfElse {

	public static void main(String[] args) {
		// Possible inputs
		//String name = null;
		 String name = "Java";

		//Before Java8
		String output1 = null;
		if(name != null) {
			output1 = name;
		} else {
			output1 = "";
		}
		
		//After Java8
		String output2 = Optional.ofNullable(name).orElse("");

		System.out.println("Before java 8 => " + output1);
		System.out.println("After java 8 => " + output2);
	}
}
// Expected output
//Before java 8 => Java
//After java 8 => Java
package com.optional.master;

import java.util.Optional;

public class FilterInsteadIf {

	public static void main(String[] args) {
		String input = "filter";

		//Before Java8
		String output1 = null;
		if(input.length() > 3) {
			output1 = input.substring(0,3);
		}
		System.out.println("Before Java 8 : " + output1);

		//After Java8
		String output2 = Optional.ofNullable(input) // Always start from Optional, though its value never be null
				.filter(temp -> temp.length() > 3) // replacement for 'if'
				// any name -> any statement or function call which returns boolean
				.map(out -> out.substring(0, 3))
				// any name -> any statement or function call which has return value
				.orElse(null); // terminating stream
		System.out.println("After Java 8 : " + output2);
	}
}
//Expected output
//Before Java 8 : fil
//After Java 8 : fil
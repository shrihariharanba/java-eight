package com.optional.master;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ActIfPresent {
	public static void main(String[] args) {
		List<String> names = Arrays.asList("Java", "Spring", "Boot");

		// Before Java8
		if (names != null) {
			Collections.sort(names);
			System.out.println("Before java 8 => " + names);
		}

		// After Java8
		Optional.ofNullable(names).ifPresent(nameList -> { // any java statement to execute with no return
			Collections.sort(nameList);
			System.out.println("After java 8 => " + nameList);
		});
	}
}
// Expected output
// Before java 8 => [Boot, Java, Spring]
// After java 8 => [Boot, Java, Spring]
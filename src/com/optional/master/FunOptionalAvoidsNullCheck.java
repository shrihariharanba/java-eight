package com.optional.master;

import java.util.Optional;

public class FunOptionalAvoidsNullCheck {

	public static void main(String[] args) {
		Integer testNumber = 2;
		// Integer testNumber = null;

		// Before Java8
		if (testNumber != null) {
			System.out.println("Before java 8");
		}
		
		// After Java8
		if (Optional.ofNullable(testNumber).isPresent()) { // just to check is not null
			System.out.println("After java 8");
		}

	}

}

// Expected
// Before java 8
// After java 8
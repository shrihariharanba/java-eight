package com.function.master;

import java.util.Optional;

public class OptionalWithFilter {

    public static void main(String[] args) {
        String input = "thisissampletext";
        System.out.println("Before java 8 => " + limitStringBeforeJava8(input, 5));
        System.out.println("After java 8 => " + limitStringAfterJava8(input, 5));
    }
    
    private static String limitStringBeforeJava8(String input, Integer limit) {
        String out = null;
        if(input != null && input.length() > limit) {
            out = input.substring(0, limit).toUpperCase();
        } else {
            out = "";
        }
        return out;
    }
    
    private static String limitStringAfterJava8(String input, Integer limit) {
        return Optional.ofNullable(input)
                       .filter(b->b.length() > limit)
                       .map(e-> input.substring(0, limit))
                       .map(String::toUpperCase)
                       .orElse("");
    }
}
//Expected output
//Before java 8 => THISI
//After java 8 => THISI

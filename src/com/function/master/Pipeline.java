package com.function.master;

import java.util.Optional;

public class Pipeline {

    public static void main(String[] args) {
        //Possibles inputs
        Integer input = 12345678;
        //Integer input = 123456789;
        //Integer input = 1234567891;
        //Integer input = null;

        System.out.println("Before java8 : " + beforeJava8(input));
        System.out.println("After java8 : " + afterJava8(input));
    }
    
    // Some sample code
    public static Integer beforeJava8(Integer input) {
        Integer result;
        if (input != null) {
            int noOfZeros = 9 - (String.valueOf(input).length());
            Double multiplier = Math.pow(10, noOfZeros);
            result = input * Math.max(multiplier.intValue(), 1);
        } else {
            result = null;
        }
        return result;
    }

    // re-written in Java8
    public static Integer afterJava8(Integer input) {
       return Optional.ofNullable(input)
        .map(e-> Integer.valueOf(9 - (String.valueOf(e).length())))
        .map(noOfZeros->Math.pow(10, noOfZeros))
        .map(e-> input * Math.max(e.intValue(), 1))
        .orElse(null);
    }
}
// Expected output
//Before java8 : 123456780
//After java8 : 123456780

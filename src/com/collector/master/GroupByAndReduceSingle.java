package com.collector.master;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupByAndReduceSingle {

    public static void main(String[] args) {
       Map<String, Integer> input = getList().parallelStream()
              .collect(Collectors.groupingBy(FourthVegetables::getItem, Collectors.summingInt(FourthVegetables::getWeight)));
       System.out.println("Total Kgs in each item follows");
       input.entrySet().stream().forEach(e -> {
           System.out.println(e.getKey() + "\t: " + e.getValue());
       });
    }

    public static List<FourthVegetables> getList() {
        List<FourthVegetables> input = new ArrayList<>();
        input.add(new FourthVegetables("Tomato", 14));
        input.add(new FourthVegetables("Potato", 23));
        input.add(new FourthVegetables("Onion", 40));
        input.add(new FourthVegetables("Tomato", 28));
        input.add(new FourthVegetables("Onion", 39));
        return input;
    }
}
// Expected output
//Total Kgs in each item follows
//Potato  : 23
//Onion   : 79
//Tomato  : 42

class FourthVegetables {

    private String item;
    private Integer weight;

    public FourthVegetables(String item, Integer weight) {
        this.item = item;
        this.weight = weight;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

}
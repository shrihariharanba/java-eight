package com.collector.master;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupByAndReduce {

    public static void main(String[] args) {
       Map<String, List<ThirdVegetables>> input = getList().parallelStream()
                                                      .collect(Collectors.groupingBy(ThirdVegetables::getItem));
       System.out.println("Total Kgs in each item follows");
       input.entrySet().stream().forEach(e -> {
           System.out.println(e.getKey() + "\t: " + e.getValue().stream()
                                                                .map(ThirdVegetables::getWeight)
                                                                .reduce(0, (a,b) -> a + b));
       });
    }

    public static List<ThirdVegetables> getList() {
        List<ThirdVegetables> input = new ArrayList<>();
        input.add(new ThirdVegetables("Tomato", 14));
        input.add(new ThirdVegetables("Potato", 23));
        input.add(new ThirdVegetables("Onion", 40));
        input.add(new ThirdVegetables("Tomato", 28));
        input.add(new ThirdVegetables("Onion", 39));
        return input;
    }
}
// Expected output
//Total Kgs in each item follows
//Potato  : 23
//Onion   : 79
//Tomato  : 42

class ThirdVegetables {

    private String item;
    private Integer weight;

    public ThirdVegetables(String item, Integer weight) {
        this.item = item;
        this.weight = weight;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

}